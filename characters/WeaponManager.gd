extends Node3D

signal weapon_changed
signal update_ammo
signal update_weapon_stack

@export var animation_player: AnimationPlayer
@onready var bullet_point = get_node("%bullet_point")

var debug_bullet = preload("res://resources/bullet_debug.tscn")
var current_weapon = null
var weapon_stack = []
var weapon_indicator = 0
var next_weapon: String
var weapon_list = {}
var collision_exclusion = []

@export var _weapon_resources: Array[Weapon_Resource]
@export var start_weapons: Array[String]

enum {NULL, HITSCAN, PROJECTILE}

func _ready() -> void:
	animation_player.animation_finished.connect(_on_animation_player_animation_finished)
	initialize(start_weapons)
	
func _input(event: InputEvent) -> void:
	if event.is_action_pressed("weapon_up"):
		var getref = weapon_stack.find(current_weapon.weapon_name)
		getref = min(getref + 1, weapon_stack.size() - 1 )
		exit(weapon_stack[getref])
		
	if event.is_action_pressed("weapon_down"):
		var getref = weapon_stack.find(current_weapon.weapon_name)
		getref = max(getref - 1,0 )
		exit(weapon_stack[getref])
		
	if event.is_action_pressed("weapon_shoot"):
		shoot()
		
	if event.is_action_pressed("weapon_reload"):
		reload()
		
	if event.is_action_pressed("drop"):
		drop(current_weapon.weapon_name)

	if event.is_action_pressed("melee"):
		melee()
		
func initialize(_start_weapons:Array):
	for weapon in _weapon_resources:
		weapon_list[weapon.weapon_name] = weapon
		
	for i in _start_weapons:
		weapon_stack.push_back(i)
	
	current_weapon = weapon_list[weapon_stack[0]]
	emit_signal("update_weapon_stack", weapon_stack)
	enter()
	
func enter():
	animation_player.queue(current_weapon.activate_anim)
	emit_signal("weapon_changed", current_weapon.weapon_name)
	emit_signal("update_ammo", [current_weapon.current_ammo, current_weapon.reserve_ammo])
	
func exit(_next_weapon: String):
	if _next_weapon != current_weapon.weapon_name:
		if animation_player.get_current_animation() != current_weapon.deactivate_anim:
			animation_player.play(current_weapon.deactivate_anim)
			next_weapon = _next_weapon
	
func change_weapon(weapon_name:String):
	current_weapon = weapon_list[weapon_name]
	next_weapon =""
	enter()

func _on_animation_player_animation_finished(anim_name: StringName) -> void:
	if anim_name == current_weapon.deactivate_anim:
		change_weapon(next_weapon)
		
	if(anim_name == current_weapon.shoot_anim && current_weapon.autofire == true):
		if(Input.is_action_pressed("weapon_shoot")):
			shoot()
			
	if anim_name == current_weapon.reload_anim:
		calculate_reload()

func shoot():
	if current_weapon.current_ammo != 0 :
		if !animation_player.is_playing():
			animation_player.play(current_weapon.shoot_anim)
			current_weapon.current_ammo -= 1
			emit_signal("update_ammo", [current_weapon.current_ammo, current_weapon.reserve_ammo])
			var camera_collision = get_camera_collision(current_weapon.weapon_range)
			match current_weapon.type:
				NULL:
					print("weapon type not chosen")
				HITSCAN:
					hit_scan_collision(camera_collision[1])
				PROJECTILE:
					launch_projectile(camera_collision[1])
	else:
		reload()
		
func reload():
	if current_weapon.current_ammo == current_weapon.magazine:
		return
	elif !animation_player.is_playing():
		if current_weapon.reserve_ammo != 0:
			animation_player.play(current_weapon.reload_anim)
		else:
			animation_player.play(current_weapon.out_of_ammo_anim)
	
func calculate_reload():
	var reload_amount = min(current_weapon.magazine - current_weapon.current_ammo, current_weapon.magazine, current_weapon.reserve_ammo)
	current_weapon.current_ammo = current_weapon.current_ammo + reload_amount
	current_weapon.reserve_ammo = current_weapon.reserve_ammo - reload_amount
	emit_signal("update_ammo", [current_weapon.current_ammo, current_weapon.reserve_ammo])
	
func melee():
	if animation_player.get_current_animation() != current_weapon.melee_anim:
		animation_player.play(current_weapon.melee_anim)
		var camera_collision = get_camera_collision(current_weapon.melee_range)
		if camera_collision[0]:
			var direction = ((camera_collision[1] - owner.global_transform.origin).normalized())
			hit_scan_damage(camera_collision[0], direction, camera_collision[1], current_weapon.melee_damage)
	
func get_camera_collision(_weapon_range) -> Array:
	var camera = get_viewport().get_camera_3d()
	var viewport = get_viewport().get_size()
	var ray_origin = camera.project_ray_origin(viewport/2) #начало луча сканирования
	var ray_end = ray_origin + camera.project_ray_normal(viewport/2) * _weapon_range #конец луча сканирования
	var new_intersection = PhysicsRayQueryParameters3D.create(ray_origin, ray_end)
	new_intersection.set_exclude(collision_exclusion)
	var intersection = get_world_3d().direct_space_state.intersect_ray(new_intersection)
	
	if not intersection.is_empty():
		var hit_indicator = debug_bullet.instantiate()
		var world  = get_tree().get_root().get_child(0)
		world.add_child(hit_indicator)
		hit_indicator.global_translate(intersection.position)
		var col_point = [intersection.collider, intersection.position]
		return col_point
	else:
		return [null, ray_end]
	
func hit_scan_collision(collision_point):
	var bullet_direction = (collision_point - bullet_point.get_global_transform().origin).normalized()
	var new_intersection = PhysicsRayQueryParameters3D.create(bullet_point.get_global_transform().origin, collision_point + bullet_direction*2)
	var bullet_collision = get_world_3d().direct_space_state.intersect_ray(new_intersection)
	if bullet_collision:
		hit_scan_damage(bullet_collision.collider, bullet_direction, bullet_collision.position, current_weapon.damage)

func hit_scan_damage(collider, direction, position, _weapon_damage):
	if collider.is_in_group("target") and collider.has_method("hit_successful"):
		collider.hit_successful(_weapon_damage, direction, position)

func launch_projectile(point: Vector3):
	var direction = (point - bullet_point.get_global_transform().origin).normalized()
	var projectile = current_weapon.projectile_to_load.instantiate()
	var projectile_RID = projectile.get_rid()
	collision_exclusion.push_front(projectile_RID)
	projectile.tree_exited.connect(remove_exclusion.bind(projectile.get_rid()))
	
	bullet_point.add_child(projectile)
	projectile.damage = current_weapon.damage
	projectile.set_linear_velocity(direction * current_weapon.projectile_velocity)
	
func remove_exclusion(projectile_rid):
	collision_exclusion.erase(projectile_rid)


func _on_puckup_area_body_entered(body):
	if body.pickup_ready:
		var weapon_in_stack = weapon_stack.find(body.weapon_name, 0)
		
		if weapon_in_stack == -1 && body.pickup_type == "WEAPON":
			var getref = weapon_stack.find(current_weapon.weapon_name)
			weapon_stack.insert(getref, body.weapon_name)
			weapon_list[body.weapon_name].current_ammo = body.current_ammo
			weapon_list[body.weapon_name].reserve_ammo = body.reserve_ammo
		
			emit_signal("update_weapon_stack", weapon_stack)
			exit(body.weapon_name)
			body.queue_free()
		else:
			var remaining = add_ammo(body.weapon_name, body.current_ammo + body.reserve_ammo)
			if remaining  == 0:
				body.queue_free()
		
			body.current_ammo = min(remaining, weapon_list[body.weapon_name].magazine)
			body.reserve_ammo = max ((remaining - body.current_ammo),0)

func drop(_name: String):
	if weapon_list[_name].can_be_dropped && weapon_stack.size() != 1:
		var weapon_ref = weapon_stack.find(_name, 0)
		if weapon_ref != -1:
			weapon_stack.pop_at(weapon_ref)
			emit_signal("update_weapon_stack", weapon_stack)
			
			var weapon_dropped = weapon_list[_name].weapon_drop.instantiate()
			weapon_dropped.current_ammo = weapon_list[_name].current_ammo
			weapon_dropped.reserve_ammo = weapon_list[_name].reserve_ammo
			weapon_dropped.set_global_transform(bullet_point.get_global_transform())
			var world = get_tree().get_root().get_child(0)
			world.add_child(weapon_dropped)
			var getref = weapon_stack.find(current_weapon.weapon_name)
			getref = max (getref - 1, 0)
			
			exit(weapon_stack[getref])
			
func add_ammo(_weapon: String , ammo: int) -> int:
	var _weaponstat = weapon_list[_weapon]
	
	var required = _weaponstat.max_ammo - _weaponstat.reserve_ammo
	var remaining = max((ammo - required), 0)
	
	_weaponstat.reserve_ammo += min(ammo, required)
	emit_signal("update_ammo", [current_weapon.current_ammo, current_weapon.reserve_ammo])
	return remaining
