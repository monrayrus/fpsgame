extends RigidBody3D

@export var weapon_name: String
@export var current_ammo: int
@export var reserve_ammo: int
@export_enum("WEAPON", "AMMO") var pickup_type: String = "WEAPON"

var pickup_ready: bool = false

func _ready():
	await get_tree().create_timer(2).timeout
	pickup_ready = true
